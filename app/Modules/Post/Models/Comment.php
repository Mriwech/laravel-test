<?php

namespace App\Modules\Post\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    public $timestamps = true;


    protected $table = 'comments';


    protected $fillable = [
        'description',
        'user_id',
        'post_id',
    ];

    public function user(){
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

    public function post(){
        return $this->hasOne('App\Modules\Post\Models\Post', 'id', 'post_id');
    }


}
