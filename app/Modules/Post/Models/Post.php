<?php

namespace App\Modules\Post\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    public $timestamps = true;


    protected $table = 'posts';


    protected $fillable = [
        'title',
        'content',
        'user_id',
    ];

    public function user(){
        return $this->hasOne('App\Modules\User\Models\User', 'id', 'user_id');
    }

    public function comments(){
        return $this->hasMany('App\Modules\Post\Models\Comment', 'post_id', 'id');

    }


}
