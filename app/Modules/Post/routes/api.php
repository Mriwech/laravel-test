<?php

Route::group(['module' => 'Post', 'middleware' => ['api'], 'namespace' => 'App\Modules\Post\Controllers'], function() {

    Route::resource('Post', 'ApiPostController');

    Route::post('/api/post', 'ApiPostController@apiHandleCreatePost')->name('apiHandleCreatePost');
    Route::get('/api/posts/{page?}', 'ApiPostController@apiGetPosts')->name('apiGetPosts');
    Route::get('/api/post/{id}', 'ApiPostController@apiGetPost')->name('apiGetPost');
    Route::post('/api/post/{id}/comment', 'ApiPostController@apiHandlePostComment')->name('apiHandlePostComment');

    Route::match(['put', 'patch'], '/api/post/{id}', 'ApiPostController@apiHandleUpdatePost')->name('apiHandleUpdatePost');

});
