<?php

namespace App\Modules\Post\Controllers;

use App\Modules\Post\Models\Comment;
use App\Modules\Post\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiPostController extends Controller
{


    public function apiHandleCreatePost(Request $request){

        //TODO Check token

        if(
            !$request->has('title') or
            !$request->has('content') or
            !$request->has('userId')
        )
            return response()->json(['status' => 201]);

        $post = Post::create([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'user_id' => $request->input('userId')
        ]);

        if($post)
            return response()->json(['status' => 200, 'post' => $post]);
        else
            return response()->json(['status' => 202]);
    }

    public function apiGetPosts($page = null){

        //TODO Check token

        $posts = Post::with('user')
            ->when($page, function($query) use($page){
                $query->skip(($page - 1) * 20) //When we want to create pagination of 20 post peer page
                ->take(20);
            })
            ->get();

        if($posts)
            return response()->json(['status' => 200, 'posts' => $posts]);
        else
            return response()->json(['status' => 202]);
    }

    public function apiGetPost($id){

        //TODO Check token

        $post = Post::where('id',$id)
                ->with('user')
                ->with('comments')
                ->get();

        if($post)
            return response()->json(['status' => 200, 'posts' => $post]);
        else
            return response()->json(['status' => 202]);
    }

    public function apiHandlePostComment(Request $request, $id){

        //TODO Check token

        if(
            !$request->has('description') or
            !$request->has('userId') or
            !$id
        )
            return response()->json(['status' => 201]);

        $comment = Comment::create([
            'description' => $request->input('description'),
            'user_id' => $request->input('userId'),
            'post_id' => $id
        ]);

        if($comment)
            return response()->json(['status' => 200, 'comment' => $comment]);
        else
            return response()->json(['status' => 202]);
    }

    public function apiHandleUpdatePost(Request $request, $id){

        //TODO Check token

        if(
            !$id or
            !$request->has('title') or
            !$request->has('content') or
            !$request->has('userId')
        )
            return response()->json(['status' => 201]);

        $post = Post::where('id', $id)
            ->update([
                'title' => $request->input('title'),
                'content' => $request->input('content'),
                'user_id' => $request->input('userId')
            ]);

        if($post)
            return response()->json(['status' => 200, 'post' => Post::find($id)]);
        else
            return response()->json(['status' => 202]);
    }

}
