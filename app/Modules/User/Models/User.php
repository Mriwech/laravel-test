<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

    public $timestamps = true;


    protected $table = 'users';


    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password'
    ];

    public function posts(){
        return $this->hasMany('App\Modules\Post\Model\Post', 'user_id', 'id');
    }

    public function comments(){
        return $this->hasMany('App\Modules\Post\Model\Comment', 'user_id', 'id');
    }

}
