<?php

use App\Modules\User\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Marouéne',
            'last_name' => 'TAYARI',
            'email' => 'tayari_marouene@yahoo.fr',
            'password' => bcrypt('123456')
        ]);
    }
}
