<?php

use App\Modules\Post\Models\Comment;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Comment::create([
            'description' => 'Nice post i like it !',
            'user_id' => 1,
            'post_id' => 1
        ]);
    }
}
