## Projet : Laravel Test

## Chargés
- [Tayari Marouéne](mailto:tayari_marouene@yahoo.fr), Développeur

## Versions
- PHP 7.0
- Laravel 5.5

## IDE
- PHPStorm 2017.3

## Requirements
- PHP 7.0 ou >7.0
- Composer
- MySql

## Instalation

- Pour cloner le projet : Git clone git@gitlab.com:Mriwech/laravel-test.git
- Par la suite se rendre dans la repertoir du projet et tapper la commande composer install
- Après l'installation du composer vous devez créer la base de données "test project" et assurez-vous que vous avez l'utilisateur Root avec tous les prèvilége de création et suppression.
- Lancer la commande php artisan migrate --seed
- Have fun.
- If you have any issue with installation pelase contact me at 53 959 437
 


